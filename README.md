## Additional Keypad for Traktor DJ Software

Licensed under GPL3

This project adds an additional self made keypad to the typical workflow of a DJ so that samples, loops, filters or other settings can be adjusted by pressing a button. It contains the following parts:

1. Host script
2. Arduino code
3. Hardware part list
4. Hardware layout


![picture alt](https://i.imgur.com/yMvxOO7.jpg "Hardware Keypad built for this project")

- - - -

# Host script
Since the Arduino Nano v3 I had lying around does not support [HID](https://en.wikipedia.org/wiki/USB_human_interface_device_class), I had to write a small script which connects to a COM port, checks if a fitting device is connected and then starts to take the input of the COM port and turn it into simulated keystrokes. **Only compatible to Windows.**

Usage: **py ComTranslator.py** will look like this:

```
ComTranslator v0.2 2017-03-17

Available COM Ports:

	COM1 - Kommunikationsanschluss (COM1)
	COM4 - USB-SERIAL CH340 (COM4)

Found Device USB-SERIAL CH340 on COM4
Connected to COM4
```


# Arduino Code
This code is a modified version of the Arduino Library Keypad: http://playground.arduino.cc/Code/Keypad
You will need to download this library if you wish to use this code.

This implementation maps a keypad of 3 x 3 buttons with these alpha keys:

a  b  c 
d  e  f
g  h  i

You can find the sketch under EventKeypad/


# Hardware part list

The Bill of Material of the components you will need:

* 1x Arduino Nano
* 1x Case (At least 10cm x 15cm, must hold all buttons and the Arduino)
* 9x Tactile Switch Buttons (With mounting option)
* 1x Prototype PCB
* Some wire

Additional tools:

* Solder station
* Solder tin

# Hardware layout

Solder your buttons, wire and Arduino onto your PCB to fit the following schematics:

![picture alt](https://i.imgur.com/7UEVQfv.png "Schematics for the keypad")

