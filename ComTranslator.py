#!/usr/bin/env python
import serial
import serial.tools.list_ports
import keyboard
import datetime
import time
import os

version = '0.2 2017-03-17'

def userComPortChoose():
    """

    Reads available COM ports
     autoconnects to a certain device with ID USB-SERIAL CH340 (A counterfeit Arduino Nano V3)

     If no such arduino is to be found, ask the user which port to connect to.
     Prompts the user again if the input was invalid.

     """
    print('Available COM Ports:\n\r')
    availableDevices = serial.tools.list_ports.comports()
    for dev in availableDevices:
        print('\t', dev)

    if 'USB-SERIAL CH340' in dev.description:
        print('\nFound Device USB-SERIAL CH340 on', dev.device)
        return dev.device

    userInput = input('\nCould not automatically find device, choose the port of your device (r for rescan): ')

    try:
        chosenPort = int(userInput)
        return 'COM' + str(chosenPort)
    except ValueError:
        if 'r' != str(userInput):
            print('Invalid input, try again')
        return userComPortChoose()  # Possible stackoverflow due to user input, fix?


def openCOMPort(port):
    """

    Opens a serial COM port to the given port number

    """

    try:
        ser = serial.Serial(
            port = port,
            baudrate = '9600',
            parity = serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize = serial.EIGHTBITS,
            timeout = 0
        )

        print('Connected to', port)
        return ser

    except(OSError, serial.SerialException):
        print('Could not connect to device')
        exit()

def banner():
    print("ComTranslator v%s\n\r" % version)


if __name__ == '__main__':
    banner()
    if not 'nt' in os.name:
        print('ComTranslator only supports Windows! Exiting...')
        exit()
    port = userComPortChoose()
    connection = openCOMPort(port)
    try:
        while True:
            for line in connection.read():
                if line > 13: # We do not want any COM overhead, only the ASCII symbol
                    print(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'),
                          'Pressed ' + chr(line)[0])
                    keyboard.press_and_release(chr(line)[0])
    except serial.serialutil.SerialException:
        print('Device disconnected')

    connection.close()
